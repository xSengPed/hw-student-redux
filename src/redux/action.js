import axios from 'axios'

export const Change = (key,value) => {
    return {
        type : 'CHANGE',
        key: key,
        value : value
    }
}

export const addStudent = (form) => async (dispatch) => {
    console.log(form)
    const result = await axios.post(`http://localhost/api/students`,form)
    dispatch({
        type : 'ADD_STUDENT',
        student : { 
            ...form,
        }
    })
    dispatch({
        type: 'INITIAL'
    })
    console.log('Students has been added')
    
}

export const deleteStudent = (id) => async (dispatch) => {

    const result = await axios.delete(
      `http://localhost/api/students/${id}`
    );
    dispatch({ 
        type: "DELETE_STUDENTS", 
        id: id });
  }

export const updateStudent = (id, form) => async (dispatch) => {

    const result = await axios.put(`http://localhost/api/students/${id}`, form)
    dispatch({
        type: 'UPDATE_STUDENTS',
        id: id,
        student : { ...form, id: id }
    })

}