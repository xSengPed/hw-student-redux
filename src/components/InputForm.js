import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Input, Row, Col, Button, InputNumber } from "antd";
import "./input.css";
import axios from "axios";
import { Change, addStudent } from "../redux/action";
import { bindActionCreators } from "redux";

const InputForm = props => {
    const getStudents = async () => {
        const result = await axios.get(`http://localhost/api/students`);
        const action = {type : "GET_STUDENTS",students : result.data}
        dispatch(action)
    }
    const dispatch = useDispatch();
    const form = useSelector(state => state.form);
    const students = useSelector(state => state.students);
    const add = async () => {
        await act.addStudent(form)
            console.log('Add button pressed')
        await getStudents()
    }
    const act = bindActionCreators(
        {
          Change: Change,
          addStudent: addStudent
        },
        useDispatch()
    )


  const { data, onChange } = props;

  return (
    <div className="container">
      <div className="frame-border card">
        <p id="header">
          <h2>Create Student</h2>
        </p>
        <p>
          <Input
            
            onChange={e => act.Change("name", e.target.value)}
            placeholder="Name"
          />
        </p>
        <p>
          <Input
            
            onChange={e => act.Change("surname", e.target.value)}
            placeholder="Surname"
          />
        </p>
        <p>
          <Input
            
            onChange={e => act.Change("major", e.target.value)}
            placeholder="Major"
          />
        </p>
        <p>
          GPA :{" "}
          <Input
            
            onChange={e => act.Change("gpa", e.target.value)}
          />
        </p>
        <div id="btn">
          <Button onClick={()=>{add()}} type="primary">
            Create
          </Button>{" "}
        </div>
      </div>
      <div></div>
    </div>
  );
};
export default InputForm;
